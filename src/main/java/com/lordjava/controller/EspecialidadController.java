package com.lordjava.controller;

import com.lordjava.model.Especialidad;
import com.lordjava.service.IEspecialidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/especialidad")
public class EspecialidadController {

    @Autowired
    private IEspecialidadService service;

    @PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Especialidad> registrar(@RequestBody Especialidad especialidad){
        Especialidad esp = new Especialidad();
        try{
            esp = service.registrar(especialidad);
        }catch (Exception e){
            return  new ResponseEntity<Especialidad>(esp, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Especialidad>(esp, HttpStatus.OK);
    }
}
