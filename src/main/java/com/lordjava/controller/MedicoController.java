package com.lordjava.controller;

import com.lordjava.model.Medico;
import com.lordjava.model.Paciente;
import com.lordjava.service.IMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/medico")
public class MedicoController {

    @Autowired
    private IMedicoService service;

    @GetMapping(value = "/listar", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Medico>> listar(){
        List<Medico> medicos = new ArrayList<>();
        try{
            medicos = service.listar();
        }catch (Exception e){
            return new ResponseEntity<List<Medico>>(medicos, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<List<Medico>>(medicos, HttpStatus.OK);
    }

    @GetMapping(value = "/listarId/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Medico> listarId(@PathVariable("id") Integer id){
        Medico medico = new Medico();
        try{
            medico = service.listarId(id);
        }catch (Exception e){
            return  new ResponseEntity<Medico>(medico, HttpStatus.INTERNAL_SERVER_ERROR);
        }
            return new ResponseEntity<Medico>(medico, HttpStatus.OK);
    }

    @PostMapping(value = "/registrar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Medico> registrar(@RequestBody Medico medico){
        Medico mec = new Medico();
        try{
            mec = service.registrar(medico);
        }catch (Exception e){
            return new ResponseEntity<Medico>(mec, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Medico>(mec, HttpStatus.OK);
    }

    @PutMapping(value = "/actualizar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> actualizar(@RequestBody Medico medico){
        int resultado = 0;
        try{
            service.actualizar(medico);
            resultado = 1;
        }catch (Exception e){
            resultado = 0;
        }
        return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
    }

    @DeleteMapping(value = "/eliminar/id", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Integer> eliminar(@PathVariable Integer id){
        int resultado = 0;
        try{
            service.eliminar(id);
            resultado = 1;
        }catch (Exception e){
            resultado = 0;
        }
        return new ResponseEntity<Integer>(resultado, HttpStatus.OK);
    }


}
