package com.lordjava.dao;

import com.lordjava.model.Examen;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IExamenDAO extends JpaRepository<Examen, Integer> {

}
