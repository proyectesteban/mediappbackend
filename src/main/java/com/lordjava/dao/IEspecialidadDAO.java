package com.lordjava.dao;

import com.lordjava.model.Especialidad;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEspecialidadDAO extends JpaRepository<Especialidad, Integer> {

}
