package com.lordjava.dao;

import com.lordjava.model.Consulta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IConsultaDAO extends JpaRepository<Consulta, Integer> {

}
