package com.lordjava.dao;

import com.lordjava.model.Medico;
import org.springframework.data.jpa.repository.JpaRepository;

//@Repository
public interface IMedicoDAO extends JpaRepository<Medico, Integer> {// un generico  no puede tener un primitivo(int); mininmo un wraper(Integer)
    // hacemos extends al JpaRep... que crea el crud automatico del
    // objeto (Paciente)y le pasamos el valor de su llave primaria(Integer)
}
