package com.lordjava.dao;

import com.lordjava.model.Paciente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//@Repository
public interface IPacienteDAO extends JpaRepository<Paciente, Integer> {// un generico  no puede tener un primitivo(int); mininmo un wraper(Integer)
    // hacemos extends al JpaRep... que crea el crud automatico del
    // objeto (Paciente)y le pasamos el valor de su llave primaria(Integer)
}
