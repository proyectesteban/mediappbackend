package com.lordjava.service;

import com.lordjava.model.Paciente;

import java.util.List;

public interface IPacienteService {

    Paciente registrar(Paciente paciente);
    void actualizar(Paciente paciente);
    void eliminar(int idPaciente);
    List<Paciente> listar();
    Paciente listarId(int idPaciente);
}
