package com.lordjava.service;

import com.lordjava.model.Consulta;

import java.util.ArrayList;
import java.util.List;

public interface IConsultaService {

    Consulta registrar(Consulta consulta);

    public void atualizar(Consulta consulta);

    public void eliminar(Integer idConsulta);

    List<Consulta> listar();

    Consulta listarId(Integer idConsulta);
}
