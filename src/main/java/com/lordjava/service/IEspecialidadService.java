package com.lordjava.service;

import com.lordjava.model.Especialidad;

import java.util.List;

public interface IEspecialidadService {

    Especialidad registrar(Especialidad especialidad);
    void actualizar(Especialidad especialidad);
    void eliminar(int idEspecialidad);
    List<Especialidad> listar();
    Especialidad listarId(int idEspecialidad);
}
