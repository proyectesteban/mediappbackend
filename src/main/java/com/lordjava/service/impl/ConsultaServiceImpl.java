package com.lordjava.service.impl;

import com.lordjava.dao.IConsultaDAO;
import com.lordjava.model.Consulta;
import com.lordjava.model.DetalleConsulta;
import com.lordjava.service.IConsultaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsultaServiceImpl implements IConsultaService {

    @Autowired
    private IConsultaDAO dao;

    @Override
    public Consulta registrar(Consulta consulta) {
        // Esto es Java 7
        /*for(DetalleConsulta detalle:consulta.getDetalleConsultas()){
            detalle.setConsulta(consulta);
        }

        consulta.getDetalleConsultas().forEach(x -> {
            x.setConsulta(consulta);
        });*/

        // Esto es Java 8
        consulta.getDetalleConsultas().forEach(x -> x.setConsulta(consulta));
        return dao.save(consulta);
    }

    @Override
    public void atualizar(Consulta consulta) {
        dao.save(consulta);
    }

    @Override
    public void eliminar(Integer idConsulta) {
        dao.delete(idConsulta);
    }

    @Override
    public List<Consulta> listar() {
        return dao.findAll();
    }

    @Override
    public Consulta listarId(Integer idConsulta) {
        return dao.findOne(idConsulta);
    }
}
