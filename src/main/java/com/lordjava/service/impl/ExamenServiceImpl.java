package com.lordjava.service.impl;

import com.lordjava.dao.IEspecialidadDAO;
import com.lordjava.dao.IExamenDAO;
import com.lordjava.model.Examen;
import com.lordjava.service.IExamenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExamenServiceImpl implements IExamenService {

    @Autowired
    private IExamenDAO dao;

    @Override
    public Examen registrar(Examen examen) {
        return dao.save(examen);
    }

    @Override
    public void actualizar(Examen examen) {
        dao.save(examen);
    }

    @Override
    public void eliminar(int idExamen) {
        dao.delete(idExamen);
    }

    @Override
    public List<Examen> listar() {
        return dao.findAll();
    }

    @Override
    public Examen listarId(int idExamen) {
        return dao.findOne(idExamen);
    }
}
