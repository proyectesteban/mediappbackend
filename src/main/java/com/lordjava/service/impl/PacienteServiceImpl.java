package com.lordjava.service.impl;

import com.lordjava.dao.IPacienteDAO;
import com.lordjava.model.Paciente;
import com.lordjava.service.IPacienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PacienteServiceImpl implements IPacienteService {

    @Autowired
    private IPacienteDAO dao;

    @Override
    public Paciente registrar(Paciente paciente) {
        return dao.save(paciente);
    }

    @Override
    public void actualizar(Paciente paciente) {
        dao.save(paciente);
    }

    @Override
    public void eliminar(int idPaciente) {
        dao.delete(idPaciente);
    }

    @Override
    public List<Paciente> listar() {
        return dao.findAll();
    }

    @Override
    public Paciente listarId(int idPaciente) {
        return dao.findOne(idPaciente);
    }

    /*@Override
    public Paciente listarId(int idPaciente) {
        return dao.findById(idPaciente).orElse(null);
    }*/

}
