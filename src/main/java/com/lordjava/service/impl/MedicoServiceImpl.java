package com.lordjava.service.impl;

import com.lordjava.dao.IMedicoDAO;
import com.lordjava.model.Medico;
import com.lordjava.service.IMedicoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MedicoServiceImpl implements IMedicoService {

    @Autowired
    private IMedicoDAO dao;

    @Override
    public Medico registrar(Medico medico) {
        return dao.save(medico);
    }

    @Override
    public void actualizar(Medico medico) {
        dao.save(medico);
    }

    @Override
    public void eliminar(int idMedico) {
        dao.delete(idMedico);
    }

    @Override
    public List<Medico> listar() {
        return dao.findAll();
    }

    @Override
    public Medico listarId(int idMedico) {
        return dao.findOne(idMedico);
    }

    /*@Override
    public Paciente listarId(int idPaciente) {
        return dao.findById(idPaciente).orElse(null);
    }*/

}
