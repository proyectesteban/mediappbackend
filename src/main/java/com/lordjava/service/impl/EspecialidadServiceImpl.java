package com.lordjava.service.impl;

import com.lordjava.dao.IEspecialidadDAO;
import com.lordjava.model.Especialidad;
import com.lordjava.service.IEspecialidadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EspecialidadServiceImpl implements IEspecialidadService {

    @Autowired
    private IEspecialidadDAO dao;

    @Override
    public Especialidad registrar(Especialidad especialidad) {
        return dao.save(especialidad);
    }

    @Override
    public void actualizar(Especialidad especialidad) {
        dao.save(especialidad);
    }

    @Override
    public void eliminar(int idEspecialidad) {
        dao.delete(idEspecialidad);
    }

    @Override
    public List<Especialidad> listar() {
        return dao.findAll();
    }

    @Override
    public Especialidad listarId(int idEspecialidad) {
        return dao.findOne(idEspecialidad);
    }
}
