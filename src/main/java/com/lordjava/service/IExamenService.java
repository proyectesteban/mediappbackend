package com.lordjava.service;

import com.lordjava.model.Examen;

import java.util.List;

public interface IExamenService {

    Examen registrar(Examen examen);
    void actualizar(Examen examen);
    void eliminar(int idExamen);
    List<Examen>listar();
    Examen listarId(int idExamen);
}
