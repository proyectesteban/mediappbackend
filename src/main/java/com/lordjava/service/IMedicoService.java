package com.lordjava.service;

import com.lordjava.model.Medico;

import java.util.List;

public interface IMedicoService {

    Medico registrar(Medico medico);
    void actualizar(Medico medico);
    void eliminar(int idMedico);
    List<Medico> listar();
    Medico listarId(int idMedico);
}
